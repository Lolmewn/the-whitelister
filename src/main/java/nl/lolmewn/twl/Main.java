/*
 *  Copyright 2012 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.twl;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.twl.Updater.UpdateType;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Lolmewn <info@lolmewn.nl>
 */
public class Main extends JavaPlugin {

    private Settings settings;
    private final Set<UUID> whitelisted = new HashSet<UUID>();
    private boolean whitelistEnabled = true;
    private MySQL mysql;

    @Override
    public void onEnable() {
        if (this.getServer().getOnlineMode()) {
            this.getServer().getPluginManager().registerEvents(new OnlineMode(this), this);
        } else {
            this.getServer().getPluginManager().registerEvents(new OfflineMode(this), this);
        }
        loadConfig();
        loadPlayers();
        this.checkUpdate(this.getSettings().isUpdate());
    }

    @Override
    public void onDisable() {
        this.savePlayers();
    }

    public Settings getSettings() {
        return this.settings;
    }

    protected synchronized MySQL getMySQL() {
        return this.mysql;
    }

    public boolean isWhitelistEnabled() {
        return whitelistEnabled;
    }

    public boolean hasPlayer(String player) {
        return this.hasPlayer(this.getServer().getOfflinePlayer(player).getUniqueId());
    }

    public boolean hasPlayer(UUID uuid) {
        return this.whitelisted.contains(uuid);
    }

    private void loadConfig() {
        this.getDataFolder().mkdirs();
        File config = new File(this.getDataFolder(), "config.yml");
        if (!config.exists()) {
            //first run
            this.saveResource("config.yml", false);
            int world = loadWorld(this.getServer().getWorlds().get(0));
            int wl = loadVanillaWhitelist();
            this.getLogger().info("Added " + world + " from default world and " + wl + " from previous whitelist");
            this.settings = new Settings(this.getConfig());
            this.checkOldVersion();
            this.savePlayers();
        } else {
            this.settings = new Settings(this.getConfig());
            this.whitelistEnabled = this.getSettings().isEnableOnStartup();
            if (!this.getConfig().isSet("uuids-converted")) {
                this.getConfig().set("uuids-converted", true);
                this.saveConfig();
                Set<String> players = this.loadPlayersOld(new HashSet<String>());
                for (String player : players) {
                    this.whitelisted.add(this.getServer().getOfflinePlayer(player).getUniqueId());
                }
                if (this.getSettings().isUseMySQL()) {
                    try {
                        Connection con = this.mysql.getConnection();
                        con.createStatement().execute("DELETE FROM " + this.getSettings().getDbTable());
                        PreparedStatement st = con.prepareStatement("INSERT INTO " + this.getSettings().getDbTable() + " (player) VALUES (?)");
                        for(UUID uuid : this.whitelisted){
                            st.setString(1, uuid.toString());
                            st.addBatch();
                        }
                        st.executeBatch();
                        st.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else { 
                    this.savePlayers();
                }
            }
        }
    }

    private void loadPlayers() {
        if (this.settings.isUseMySQL()) {
            this.loadMySQL();
            if (this.getMySQL().isFault()) {
                this.settings.setIsUseMySQL(false);
                this.loadPlayers();
                return;
            }
            ResultSet set = this.getMySQL().executeQuery("SELECT * FROM " + this.getSettings().getDbTable());
            if (set == null) {
                this.getLogger().warning("Something wrong with resultset, using flatfile.");
                this.settings.setIsUseMySQL(false);
                this.loadPlayers();
                return;
            }
            try {
                while (set.next()) {
                    this.whitelisted.add(UUID.fromString(set.getString("player")));
                }
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            File file = new File(this.getDataFolder(), "players.yml");
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                return;
            }
            YamlConfiguration f = YamlConfiguration.loadConfiguration(file);
            for (String uuid : f.getStringList("players")) {
                this.whitelisted.add(UUID.fromString(uuid));
            }
        }
    }

    private Set<String> loadPlayersOld(Set<String> players) {
        if (this.settings.isUseMySQL()) {
            this.loadMySQL();
            if (this.getMySQL().isFault()) {
                this.settings.setIsUseMySQL(false);
                this.loadPlayersOld(players);
                return players;
            }
            ResultSet set = this.getMySQL().executeQuery("SELECT * FROM " + this.getSettings().getDbTable());
            if (set == null) {
                this.getLogger().warning("Something wrong with resultset, using flatfile.");
                this.settings.setIsUseMySQL(false);
                return this.loadPlayersOld(players);
            }
            try {
                while (set.next()) {
                    players.add(set.getString("player"));
                }
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            File file = new File(this.getDataFolder(), "players.yml");
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                return players;
            }
            YamlConfiguration f = YamlConfiguration.loadConfiguration(file);
            for (String uuid : f.getStringList("players")) {
                players.add(uuid);
            }
        }
        return players;
    }

    private int loadWorld(World world) {
        File folder = new File(world.getName() + File.separator + "players");
        if (!folder.exists()) {
            return -1;
        }
        int added = 0;
        for (File f : folder.listFiles()) {
            String uuid = f.getName().substring(0, f.getName().lastIndexOf("."));
            if (this.whitelisted.add(UUID.fromString(uuid))) {
                added++;
            }
        }
        return added;
    }

    private int loadVanillaWhitelist() {
        int added = 0;
        for (OfflinePlayer player : this.getServer().getWhitelistedPlayers()) {
            if (this.whitelisted.add(player.getUniqueId())) {
                added++;
            }
        }
        return added;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("=================");
            sender.sendMessage("The Whitelister");
            sender.sendMessage("Version: " + this.getDescription().getVersion());
            sender.sendMessage("Active: " + (this.whitelistEnabled ? ChatColor.GREEN + "True" : ChatColor.RED + "False"));
            sender.sendMessage("=================");
            return true;
        }
        if (args[0].equals("list")) {
            if (sender.hasPermission("wl.seelist")) {
                sender.sendMessage(ChatColor.GREEN + "Whitelisted: " + ChatColor.RED + this.whitelisted.size());
                sender.sendMessage(Arrays.toString(this.whitelisted.toArray()));
                return true;
            } else {
                sender.sendMessage("You don't have permissions to do this!");
                return true;
            }
        }
        if (args[0].equals("toggle")) {
            if (!sender.hasPermission("wl.toggle")) {
                sender.sendMessage("You don't have permissions to do this!");
                return true;
            }
            this.whitelistEnabled = !this.whitelistEnabled;
            sender.sendMessage("The whitelist is now " + (this.whitelistEnabled ? ChatColor.GREEN + "enabled" : ChatColor.RED + "disabled"));
            return true;
        }
        if (args[0].equals("on") || args[0].equals("enable")) {
            if (!sender.hasPermission("wl.toggle")) {
                sender.sendMessage("You don't have permissions to do this!");
                return true;
            }
            this.whitelistEnabled = true;
            sender.sendMessage("The whitelist is now " + (this.whitelistEnabled ? ChatColor.GREEN + "enabled" : ChatColor.RED + "disabled"));
            return true;
        }
        if (args[0].equals("off") || args[0].equals("disable")) {
            if (!sender.hasPermission("wl.toggle")) {
                sender.sendMessage("You don't have permissions to do this!");
                return true;
            }
            this.whitelistEnabled = false;
            sender.sendMessage("The whitelist is now " + (this.whitelistEnabled ? ChatColor.GREEN + "enabled" : ChatColor.RED + "disabled"));
            return true;
        }
        if (args[0].equals("remove") || args[0].equals("delete")) {
            if (!sender.hasPermission("wl.remove")) {
                sender.sendMessage("You don't have permissions to do this!");
                return true;
            }
            if (args.length == 1) {
                sender.sendMessage("Usage: /wl remove <player1> <player2> ...");
                return true;
            }
            for (int i = 1; i < args.length; i++) {
                sender.sendMessage(this.removePlayer(args[i]));
            }
            return true;
        }
        if (args[0].equals("add")) {
            if (!sender.hasPermission("wl.add")) {
                sender.sendMessage("You don't have permissions to do this!");
                return true;
            }
            if (args.length == 1) {
                sender.sendMessage("Usage: /wl add <player1> <player2> ...");
                return true;
            }
            for (int i = 1; i < args.length; i++) {
                sender.sendMessage(this.addPlayer(args[i]));
            }
            return true;
        }
        if (args[0].equalsIgnoreCase("import")) {
            if (!sender.hasPermission("wl.import")) {
                sender.sendMessage("You don't have permissions to do this!");
                return true;
            }
            if (args.length == 1) {
                sender.sendMessage("Usage: /wl import <worldName>");
                return true;
            }
            World w = this.getServer().getWorld(args[1]);
            if (w == null) {
                sender.sendMessage("World not found. Cannot import non-existing world.");
                return true;
            }
            int imported = this.loadWorld(w);
            sender.sendMessage("Imported " + imported + " new players from world " + w.getName());
            return true;
        }
        if (args[0].equalsIgnoreCase("help")) {
            if (!sender.hasPermission("wl.help")) {
                sender.sendMessage("You don't have permissions to do this!");
                return true;
            }
            sender.sendMessage("All available commands:");
            sender.sendMessage("/wl list - Prints all whitelisted players (up to ChatBox limit)");
            sender.sendMessage("/wl toggle - Turn whitelist on or off");
            sender.sendMessage("/wl add <player1> <player2> ... - Add players to the whitelist");
            sender.sendMessage("/wl remove <player1> <player2> ... - Remove players from the whitelist");
            sender.sendMessage("/wl import <worldName> - Imports all players that have ever played in world <world>");
            sender.sendMessage("/wl <player1> <player2> ... - Add players to the whitelist");
            return true;
        }
        if (!sender.hasPermission("wl.add")) {
            sender.sendMessage("You don't have permissions to do this!");
            return true;
        }
        for (String arg : args) {
            sender.sendMessage(this.addPlayer(arg));
        }
        return true;
    }

    protected String removePlayer(String player) {
        if (!this.hasPlayer(player)) {
            return player + " isn't whitelisted!";
        }
        this.whitelisted.remove(this.getServer().getOfflinePlayer(player).getUniqueId());
        if (this.settings.isUseMySQL()) {
            try {
                PreparedStatement st = this.getMySQL().getConnection().prepareStatement("DELETE FROM " + this.getSettings().getDbTable() + " WHERE player=?");
                st.setString(1, this.getServer().getOfflinePlayer(player).getUniqueId().toString());
                st.execute();
                st.close();
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                return "Exception happened, please check the logs.";
            }
        }
        return player + " unwhitelisted!";
    }

    protected String addPlayer(String player) {
        if (this.hasPlayer(player)) {
            return player + " is already whitelisted!";
        }
        this.whitelisted.add(this.getServer().getOfflinePlayer(player).getUniqueId());
        if (this.settings.isUseMySQL()) {
            try {
                PreparedStatement st = this.getMySQL().getConnection().prepareStatement("INSERT INTO " + this.getSettings().getDbTable() + "(player) VALUES (?)");
                st.setString(1, this.getServer().getOfflinePlayer(player).getUniqueId().toString());
                st.execute();
                st.close();
            } catch (SQLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                return "Exception happened, please check the logs.";
            }
        }
        return player + " whitelisted!";
    }

    private void savePlayers() {
        if (this.whitelisted.isEmpty()) {
            return;
        }
        if (!this.settings.isUseMySQL()) { //no need to save them if MySQL is used, that gets done automatically.
            File file = new File(this.getDataFolder(), "players.yml");
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            YamlConfiguration f = YamlConfiguration.loadConfiguration(file);
            try {
                String[] uuids = new String[this.whitelisted.size()];
                int index = 0;
                for (UUID uuid : this.whitelisted) {
                    uuids[index++] = uuid.toString();
                }
                f.set("players", uuids);
                f.save(file);
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadMySQL() {
        this.mysql = new MySQL(this, this.getSettings().getDbHost(),
                this.getSettings().getDbPort(), this.getSettings().getDbUser(),
                this.getSettings().getDbPass(), this.getSettings().getDbDatabase(),
                this.getSettings().getDbTable());
    }

    private void checkUpdate(boolean update) {
        if (update) {
            new Updater(this, 31703, this.getFile(), UpdateType.DEFAULT, true);
        }
    }

    private void checkOldVersion() {
        File old = new File(this.getDataFolder(), "whitelist.yml");
        if (!old.exists()) {
            return;
        }
        YamlConfiguration c = YamlConfiguration.loadConfiguration(old);
        List<String> list = c.getStringList("whitelisted");
        for (String player : list) {
            if (this.hasPlayer(player)) {
                continue;
            }
            this.addPlayer(player);
        }
        this.getLogger().info("Loaded old players from whitelist.yml! (At least, tried to ;))");
    }
}
