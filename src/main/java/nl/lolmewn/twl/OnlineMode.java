/*
 *  Copyright 2012 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.twl;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;

/**
 * @author Lolmewn <info@lolmewn.nl>
 */
public class OnlineMode implements Listener{

    private final Main plugin;
    
    public OnlineMode(Main m) {
        this.plugin = m;
    }
    
    @EventHandler
    public void join(AsyncPlayerPreLoginEvent event){
        if(plugin.isWhitelistEnabled()){
            if(!this.plugin.hasPlayer(event.getUniqueId())){
                event.disallow(Result.KICK_WHITELIST, ChatColor.translateAlternateColorCodes('&', this.plugin.getSettings().getKickMessage()));
            }
        }
    }

}
